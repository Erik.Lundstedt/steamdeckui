local lfs = require("lfs")
local lousy = require("lousy")
lousy.theme.init(lousy.util.find_config("theme.lua"))
assert(lousy.theme.get(), "failed to load theme")
local window = require("window")
local webview = require("webview")
local log_chrome = require("log_chrome")
local modes = require("modes")
local binds = require("binds")
local settings = require("settings")
local setting_chrome = require("settings_chrome")
local adblock = require("adblock")
local adblock_chrome = require("adblock_chrome")
local webinspector = require("webinspector")
local formfiller = require("formfiller")
local proxy = require("proxy")
local quickmarks = require("quickmarks")
local session = require("session")
local undoclose = require("undoclose")
local userscripts = require("userscripts")
local cmdhist = require("cmdhist")
local search = require("search")
local tabhistory = require("tabhistory")
local taborder = require("taborder")
local history = require("history")
local history_chrome = require("history_chrome")
local downloads = require("downloads")
local downloads_chrome = require("downloads_chrome")
local editor = require("editor")
local help_chrome = require("help_chrome")
local binds_chrome = require("binds_chrome")
local completion = require("completion")
local follow = require("follow")
local sel = require("select")
local follow_selected = require("follow_selected")
local go_input = require("go_input")
local go_next_prev = require("go_next_prev")
local go_up = require("go_up")
local error_page = require("error_page")
local styles = require("styles")
local image_css = require("image_css")
local tab_favicons = require("tab_favicons")
local view_source = require("view_source")
local newtab_chrome = require("newtab_chrome")
local chrome = require("chrome")
local select = require("select")
local vertabs = require("vertical_tabs")
local bookmarks = require("bookmarks")
local bookmarks_chrome = require("bookmarks_chrome")
soup.cookies_storage = (luakit.data_dir .. "/cookies.db")
local function _1_(w)
  local widgets, l, r = require("lousy.widget"), w.sbar.l, w.sbar.r
  do end (l.layout):pack(widgets.uri())
  do end (l.layout):pack(widgets.hist())
  do end (l.layout):pack(widgets.progress())
  do end (r.layout):pack(widgets.buf())
  do end (r.layout):pack(log_chrome.widget())
  do end (r.layout):pack(widgets.ssl())
  do end (r.layout):pack(widgets.tabi())
  return (r.layout):pack(widgets.scroll())
end
window.add_signal("build", _1_)
window.reuse_new_tab_pages = true
bookmarks.init()
local id = 1
bookmarks.get(id)
local newtaburl = ("http://" .. "erik.lundstedt.it" .. "/pages" .. "/newtab" .. ".html")
newtab_chrome.new_tab_src = table.concat({"<meta", " http-equiv=\"Refresh\" ", "content=\"0;", " url='", newtaburl, " '\" />"})
local function _2_(w)
  return w:reload()
end
webview.add_signal("init", _2_)
window.home_page = "https://erik.lundstedt.it"
local function _3_(w, m)
  return w:forward(m.count)
end
local function _4_(w, m)
  return w:back(m.count)
end
modes.add_binds("all", {{"<Mouse9>", "Go forward in the browser history", _3_}, {"<Mouse8>", "Go back in the browser history", _4_}})
local function _5_()
  luakit.selection.clipboard = luakit.selection.primary
  return nil
end
modes.add_binds("normal", {{"<Control-y>", "Copy selected text.", _5_}})
local function _6_()
  return os.execute("rofi-rbw")
end
modes.add_binds("all", {{"<Alt-x> b", "bitwarden", _6_}})
window.reuse_new_tab_pages = true
window.act_on_synthetic_keys = true
webview.enable_caret_browsing = true
webview.enable_smooth_scrolling = true
webview.enable_spatial_navigation = true
editor.editor_cmd = "emacsclient +{line} {file}"
local w = (not luakit.nounique and (session and session.restore()))
if w then
  for i, uri in ipairs(uris) do
    w:new_tab(uri, {switch = (i == 1)})
  end
  return nil
else
  return window.new(uris)
end
