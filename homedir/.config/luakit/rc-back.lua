require("lfs")
local function _1_(modname)
  if not package.searchpath then
    return
  else
  end
  local f = package.searchpath(modname, package.path)
  if (not f or (f:find((luakit.install_paths.install_dir .. "/"), 0, true) ~= 1)) then
    return
  else
  end
  local lf = (luakit.config_dir .. "/" .. modname:gsub("%.", "/") .. ".lua")
  if (f == lf) then
    return msg.warn(("Loading local version of '" .. modname .. "' module: " .. lf))
  elseif lfs.attributes(lf) then
    return msg.warn(("Found local version " .. lf .. " for core module '" .. modname .. "', but it won't be used, unless you update 'package.path' accordingly."))
  else
    return nil
  end
end
table.insert(package.loaders, 2, _1_)
soup.cookies_storage = (luakit.data_dir .. "/cookies.db")
local lousy = require("lousy")
lousy.theme.init(lousy.util.find_config("theme.lua"))
assert(lousy.theme.get(), "failed to load theme")
local window = require("window")
local webview = require("webview")
local log_chrome = require("log_chrome")
local function _5_(w)
  local widgets, l, r = require("lousy.widget"), w.sbar.l, w.sbar.r
  do end (l.layout):pack(widgets.uri())
  do end (l.layout):pack(widgets.hist())
  do end (l.layout):pack(widgets.progress())
  do end (r.layout):pack(widgets.buf())
  do end (r.layout):pack(log_chrome.widget())
  do end (r.layout):pack(widgets.ssl())
  do end (r.layout):pack(widgets.tabi())
  return (r.layout):pack(widgets.scroll())
end
window.add_signal("build", _5_)
local modes = require("modes")
local binds = require("binds")
local settings = require("settings")
require("settings_chrome")
local adblock = require("adblock")
local adblock_chrome = require("adblock_chrome")
local webinspector = require("webinspector")
local formfiller = require("formfiller")
local proxy = require("proxy")
local quickmarks = require("quickmarks")
local session = require("session")
local undoclose = require("undoclose")
local tabhistory = require("tabhistory")
local userscripts = require("userscripts")
local cmdhist = require("cmdhist")
local search = require("search")
local taborder = require("taborder")
local history = require("history")
local history_chrome = require("history_chrome")
local downloads_chrome = require("downloads_chrome")
require("downloads")
local help_chrome = require("help_chrome")
local binds_chrome = require("binds_chrome")
local completion = require("completion")
local follow = require("follow")
local sel = require("select")
local follow_selected = require("follow_selected")
local go_input = require("go_input")
local go_next_prev = require("go_next_prev")
local go_up = require("go_up")
local error_page = require("error_page")
local styles = require("styles")
window.reuse_new_tab_pages = true
local bookmarks = require("bookmarks")
bookmarks.init()
local id = 1
bookmarks.get(id)
local image_css = require("image_css")
local tab_favicons = require("tab_favicons")
local view_source = require("view_source")
local newtab_chrome = require("newtab_chrome")
local chrome = require("chrome")
newtab_chrome.new_tab_src = "<meta http-equiv=\"Refresh\" content=\"0; url='http://erik.lundstedt.it/html/newtab.html'\" />"
local function _6_(w)
  return w:reload()
end
webview.add_signal("init", _6_)
local function _7_(w, m)
  return w:forward(m.count)
end
local function _8_(w, m)
  return w:back(m.count)
end
modes.add_binds("all", {{"<Mouse9>", "Go forward in the browser history", _7_}, {"<Mouse8>", "Go back in the browser history", _8_}})
local function _9_()
  luakit.selection.clipboard = luakit.selection.primary
  return nil
end
modes.add_binds("normal", {{"<Control-c>", "Copy selected text.", _9_}})
window.reuse_new_tab_pages = true
window.act_on_synthetic_keys = true
webview.enable_caret_browsing = true
local editor = require("editor")
editor.editor_cmd = "kitty nvim {file} +{line}"
window.home_page = "luakit://newtab"
webview.enable_smooth_scrolling = true
webview.enable_spatial_navigation = true
local w = (not luakit.nounique and (session and session.restore()))
if w then
  for i, uri in ipairs(uris) do
    w:new_tab(uri, {switch = (i == 1)})
  end
  return nil
else
  return window.new(uris)
end
