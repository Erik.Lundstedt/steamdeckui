local lfs = require "lfs"

-- Check for lua configuration files that will never be loaded because they are
-- shadowed by builtin modules.
table.insert(package.loaders, 2, function (modname)
    if not package.searchpath then return end
    local f = package.searchpath(modname, package.path)
    if not f or f:find(luakit.install_paths.install_dir .. "/", 0, true) ~= 1 then
        return
    end
    local lf = luakit.config_dir .. "/" .. modname:gsub("%.","/") .. ".lua"
    if f == lf then
        msg.warn("Loading local version of '" .. modname .. "' module: " .. lf)
    elseif lfs.attributes(lf) then
        msg.warn("Found local version " .. lf
            .. " for core module '" .. modname
            .. "', but it won't be used, unless you update 'package.path' accordingly.")
    end
end)

soup.cookies_storage = luakit.data_dir .. "/cookies.db"

-- Load library of useful functions for luakit
local lousy = require "lousy"

-- Load users theme
-- ("$XDG_CONFIG_HOME/luakit/theme.lua" or "/etc/xdg/luakit/theme.lua")
lousy.theme.init(lousy.util.find_config("theme.lua"))
assert(lousy.theme.get(), "failed to load theme")

local window = require "window"
local webview = require "webview"

local log_chrome = require "log_chrome"

window.add_signal("build", function (w)
    local widgets, l, r = require "lousy.widget", w.sbar.l, w.sbar.r

    -- Left-aligned status bar widgets
    l.layout:pack(widgets.uri())
    l.layout:pack(widgets.hist())
    l.layout:pack(widgets.progress())

    -- Right-aligned status bar widgets
    r.layout:pack(widgets.buf())
    r.layout:pack(log_chrome.widget())
    r.layout:pack(widgets.ssl())
    r.layout:pack(widgets.tabi())
    r.layout:pack(widgets.scroll())
end)

local modes = require "modes"
local binds = require "binds"

local settings = require "settings"
require "settings_chrome"

local adblock = require "adblock"
local adblock_chrome = require "adblock_chrome"
local webinspector = require "webinspector"

local formfiller = require "formfiller"
local proxy = require "proxy"

local quickmarks = require "quickmarks"
local session = require "session"

local undoclose = require "undoclose"

local tabhistory = require "tabhistory"

local userscripts = require "userscripts"

local cmdhist = require "cmdhist"

local search = require "search"

local taborder = require "taborder"





--local vertical_tabs  = require "vertical_tabs"

local history = require "history"
local history_chrome = require "history_chrome"
local downloads_chrome =require "downloads_chrome"
require "downloads"
local help_chrome = require "help_chrome"
local binds_chrome = require "binds_chrome"

local completion = require "completion"

-- Press Control-E while in insert mode to edit the contents of the currently
-- focused <textarea> or <input> element, using `xdg-open`
--local open_editor = require "open_editor"

--local editor = require "editor"
--editor.editor_cmd = "emacsclient -nw {file} +{line}"
-- kitty -e nvim {file} +{line}"






-- NoScript plugin, toggle scripts and or plugins on a per-domain basis.
-- `,ts` to toggle scripts, `,tp` to toggle plugins, `,tr` to reset.
-- If you use this module, don't use any site-specific `enable_scripts` or
-- `enable_plugins` settings, as these will conflict.
-- require "noscript"

local follow = require "follow"
local sel = require "select"

local follow_selected = require "follow_selected"
local go_input = require "go_input"
local go_next_prev = require "go_next_prev"
local go_up = require "go_up"

-- Filter Referer HTTP header if page domain does not match Referer domain
require_web_module("referer_control_wm")

local error_page = require "error_page"

local styles = require "styles"

window.reuse_new_tab_pages=true

--local hide_scrollbars = require "hide_scrollbars"

local bookmarks = require "bookmarks"


bookmarks.init()
local id = 1
bookmarks.get(id)

local image_css = require "image_css"

local tab_favicons = require "tab_favicons"

local view_source = require "view_source"



--require "userconf"

-- Put "userconf.lua" in your Luakit config dir with your own tweaks; if this is
-- permanent, no need to copy/paste/modify the default rc.lua whenever you
-- update Luakit.
--[[
if pcall(
  function ()
    lousy.util.find_config("userconf.lua")
end) then
      msg.info("infoMessage")

  require "userconf"
end

  --]]

local newtab_chrome = require "newtab_chrome"


newtab_chrome.new_tab_src=[[<meta http-equiv="Refresh" content="0; url='http://erik.lundstedt.it:8080/html/newtab.html'" />]]



local chrome = require "chrome"





--[[
lines = io.input("/home/erik/.local/share/luakit/styles/chrome.css"):read('a')

lines="\n/*******************\n******userStyle*****\n********************/\n"..lines

chrome.stylesheet=lines

--msg.info(chrome.stylesheet)
--]]

webview.add_signal("init", function (w)
   w:reload()
end)

modes.add_binds("all", {
{"<Mouse9>","Go forward in the browser history",
 function (w, m)
   w:forward(m.count)
   --w:reload()
end
},
{"<Mouse8>","Go back in the browser history",
  function (w, m)
    w:back(m.count)
    --w:reload()
  end
}
}
)

modes.add_binds("normal", {
    { "<Control-c>", "Copy selected text.", function ()
        luakit.selection.clipboard = luakit.selection.primary
    end},
})



window.reuse_new_tab_pages=true
window.act_on_synthetic_keys=true
webview.enable_caret_browsing=true


local editor = require "editor"
editor.editor_cmd = "kitty nvim {file} +{line}"

window.home_page="luakit://newtab"
webview.enable_smooth_scrolling=true
webview.enable_spatial_navigation=true

local w = (not luakit.nounique) and (session and session.restore())
if w then
    for i, uri in ipairs(uris) do
        w:new_tab(uri, { switch = i == 1 })
    end
else
    -- Or open new window
    window.new(uris)
end

--vim: et:sw=4:ts=8:sts=4:tw=80
