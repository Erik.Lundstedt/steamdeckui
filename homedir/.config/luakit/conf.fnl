(local lfs (require :lfs))
(local lousy (require :lousy))
(lousy.theme.init (lousy.util.find_config "theme.lua"))
(assert (lousy.theme.get) "failed to load theme")

(local window (require :window))
(local webview (require :webview))
(local log-chrome (require :log_chrome))
(local modes (require :modes))
(local binds (require :binds))
(local settings (require :settings))
(local setting_chrome (require :settings_chrome))

(local adblock (require :adblock))
(local adblock-chrome (require :adblock_chrome))

(local webinspector (require :webinspector))
(local formfiller (require :formfiller))
(local proxy (require :proxy))
(local quickmarks (require :quickmarks))
(local session (require :session))
(local undoclose (require :undoclose))
(local userscripts (require :userscripts))
(local cmdhist (require :cmdhist))
(local search (require :search))

(local tabhistory (require :tabhistory))
(local taborder (require :taborder))

(local history (require :history))
(local history-chrome (require :history_chrome))

(local downloads (require :downloads))
(local downloads-chrome (require :downloads_chrome))

(local editor (require :editor))
(local help-chrome (require :help_chrome))
(local binds-chrome (require :binds_chrome))
(local completion (require :completion))
(local follow (require :follow))
(local sel (require :select))
(local follow-selected (require :follow_selected))
(local go-input (require :go_input))
(local go-next-prev (require :go_next_prev))
(local go-up (require :go_up))
;;(require-web-module :referer_control_wm)
(local error-page (require :error_page))
(local styles (require :styles))
(local image-css (require :image_css))
(local tab-favicons (require :tab_favicons))
(local view-source (require :view_source))
(local newtab-chrome (require :newtab_chrome))
(local chrome (require :chrome))
(local select  (require :select))
(local vertabs (require :vertical_tabs))

(local bookmarks (require :bookmarks))
(local bookmarks_chrome (require :bookmarks_chrome))


(set soup.cookies_storage (.. luakit.data_dir :/cookies.db))


(window.add_signal
 :build
 (fn [w]
   (let
       [
        (widgets l r)
        (values
         (require "lousy.widget")
         w.sbar.l w.sbar.r)
        ]
     (l.layout:pack (widgets.uri))
     (l.layout:pack (widgets.hist))
     (l.layout:pack (widgets.progress))

     (r.layout:pack (widgets.buf))
     (r.layout:pack (log-chrome.widget))
     (r.layout:pack (widgets.ssl))
     (r.layout:pack (widgets.tabi))
     (r.layout:pack (widgets.scroll))
     )
   )
 )




(set window.reuse_new_tab_pages true)
(bookmarks.init)

(local id 1)
(bookmarks.get id)




(local newtaburl (.. "http://" "erik.lundstedt.it" "/pages" "/newtab" ".html"))
(set newtab-chrome.new_tab_src
     (table.concat
      [
       "<meta"
       " http-equiv=\"Refresh\" "
       "content=\"0;"
       " url='" newtaburl " '\" />"
       ]
      )
     )
(webview.add_signal :init (fn [w]
                            (w:reload)))

;;(set window.home_page "luakit://newtab")
(set window.home_page "https://erik.lundstedt.it")



(modes.add_binds
 :all
 [
  [ "<Mouse9>"
    "Go forward in the browser history"
    (fn [w m]
      (w:forward m.count))]
    [ "<Mouse8>"
      "Go back in the browser history"
      (fn [w m]
        (w:back m.count))]
    ]
 )

(modes.add_binds
 :normal
 [
  [ "<Control-y>"
    "Copy selected text."
    (fn []
      (set luakit.selection.clipboard
           luakit.selection.primary))]
    ])
;;{ trigger, description, action, options }
(modes.add_binds
 :all
 [
  [ "<Alt-x> b" "bitwarden"
    (fn []
      (os.execute "rofi-rbw")
      )
   ]
 ]
 )







(set window.reuse_new_tab_pages true)
(set window.act_on_synthetic_keys true)
(set webview.enable_caret_browsing true)
(set webview.enable_smooth_scrolling true)
(set webview.enable_spatial_navigation true)


(set editor.editor_cmd "emacsclient +{line} {file}")

;;(set select.label_maker (fn [] (reverse (trim (charset "awsd")))))



(local w (and (not luakit.nounique) (and session (session.restore))))
(if w (each [i uri (ipairs uris)]
        (w:new_tab uri {:switch (= i 1)})) (window.new uris))
