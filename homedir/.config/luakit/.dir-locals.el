;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

(
 (fennel-mode . (
                 (mode . outline-minor)
;;                 (outline-regexp . "^;;;+ \\|^;;;+:\\|^;;+doc: ")
                 )
              )
                                 ;;";;; \\|(...."

 (lua-mode . (
              (mode . outline-minor)
              (outline-regexp . ".*{{{*+")
              )
           )
 )
