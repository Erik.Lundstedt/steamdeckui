#!/usr/bin/env fennel
;; -*- mode: fennel; mode: rainbow; -*-
(local bg "#242837")
(local fg "#8099ee")

;;;; theme
(local theme
       {
        :color_background bg
        :color_foreground fg
        :color_bold fg
        :color_cursor fg
        :color_cursor_foreground bg
        :color_highlight fg
        :color_highlight_foreground bg
        :color_0     bg;;"#000000"
        :color_1     "#d36265"
        :color_2     "#aece91"
        :color_3     "#e7e18c"
        :color_4     "#7a7ab0"
        :color_5     "#963c59"
        :color_6     "#418179"
        :color_7     fg;;"#bebebe"
        :color_8     "#666666"
        :color_9     "#ef8171"
        :color_10    "#e5f779"
        :color_11    "#fff796"
        :color_12    "#4186be"
        :color_13    "#ef9ebe"
        :color_14    "#71bebe"
        :color_15    "#ffffff"
        })



theme
