local tym = require("tym")
local defaultFont = "Ubuntu Mono Nerd Font 16"
local uriSchemes = {"https", "gemini", "file", "mailto"}
local unclutter = true
local cursorShapes = {block = "block", ibeam = "ibeam", underline = "underline"}
local function bgAlpha(alpha)
  local r, g, b, a = tym.color_to_rgba(tym.get("color_background"))
  return tym.rgba_to_color(r, g, b, alpha)
end
local window = {width = (80 + 0), height = (22 + 2), paddingHori = 10, paddingVert = 10}
tym.set_config({font = defaultFont, uri_schemes = table.concat(uriSchemes, " "), autohide = unclutter, cursor_shape = cursorShapes.ibeam, color_background = bgAlpha(0.8), width = window.width, height = window.height, padding_horizontal = window.paddingHoriz, padding_vertical = window.paddingVerti})
local function update_alpha(delta)
  local r, g, b, a = tym.color_to_rgba(tym.get("color_background"))
  a = math.max(math.min(1, (a + delta)), 0)
  bg = tym.rgba_to_color(r, g, b, a)
  tym.set("color_background", bg)
  return tym.notify(string.format("%s alpha to %f", (((delta > 0) and "Inc") or "Dec"), a))
end
local alphaChangeAspect = 0.05
local function _1_()
  return update_alpha(alphaChangeAspect)
end
local function _2_()
  return update_alpha(( - alphaChangeAspect))
end
tym.set_keymaps({["<Ctrl><Shift>Up"] = _1_, ["<Ctrl><Shift>Down"] = _2_})
local function _3_(dx, dy, x, y)
  if tym.check_mod_state("<Ctrl>") then
    if (dy > 0) then
      s = (tym.get("scale") - 10)
    else
      s = (tym.get("scale") + 10)
    end
    tym.set("scale", s)
    tym.notify(("set scale: " .. s .. "%"))
  else
  end
  if tym.check_mod_state("<Shift>") then
    update_alpha((((dy < 0) and alphaChangeAspect) or ( - alphaChangeAspect)))
    return true
  else
    return nil
  end
end
return tym.set_hook("scroll", _3_)
