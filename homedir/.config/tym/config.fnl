#!/usr/bin/env fennel

(local tym (require :tym))

(local defaultFont "Ubuntu Mono Nerd Font 16")
(local uriSchemes ["https" "gemini" "file" "mailto"])
(local unclutter true)
(local cursorShapes
       {
        :block "block"
        :ibeam "ibeam"
        :underline "underline"
        })


(fn bgAlpha [alpha]
  (var (r g b a) (tym.color_to_rgba (tym.get :color_background)))
  (tym.rgba_to_color r g b alpha)
  )


(local window
       {
        :width (+ 80 0)
        :height (+ 22 2)
        :paddingHori 10
        :paddingVert 10
        })

(tym.set_config
 {
  :font defaultFont
  :uri_schemes (table.concat uriSchemes " ")
  :autohide unclutter
  :cursor_shape cursorShapes.ibeam
  :color_background (bgAlpha 0.8)
  :width window.width
  :height window.height
  :padding_horizontal window.paddingHoriz
  :padding_vertical   window.paddingVerti
  })

(fn update-alpha [delta]
  (var (r g b a) (tym.color_to_rgba (tym.get :color_background)))
  (set a (math.max (math.min 1.0 (+ a delta)) 0.0))
  (global bg (tym.rgba_to_color r g b a))
  (tym.set :color_background bg)
  (tym.notify (string.format "%s alpha to %f" (or (and (> delta 0) :Inc) :Dec)
                             a)
              )
  )

(local alphaChangeAspect 0.05)

(tym.set_keymaps {:<Ctrl><Shift>Up
                  (fn []
                    (update-alpha alphaChangeAspect))
                  :<Ctrl><Shift>Down
                  (fn []
                    (update-alpha (- alphaChangeAspect))
                    )
                  }
                 )
(tym.set_hook
 :scroll
 (fn [dx dy x y]
   (when (tym.check_mod_state "<Ctrl>")
     (if (> dy 0) (global s (- (tym.get :scale) 10))
         (global s (+ (tym.get :scale) 10)
                 )
         )
     (tym.set :scale s)
     (tym.notify (.. "set scale: " s "%"))
     true)
   (when (tym.check_mod_state "<Shift>")
     (update-alpha (or (and (< dy 0) alphaChangeAspect) (- alphaChangeAspect)))
     true
     )
   )
 )
