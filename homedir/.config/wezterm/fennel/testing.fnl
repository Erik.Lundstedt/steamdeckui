#!bin/env fennel


(fn getBatIcon [battery]
  "TODO write something here"

  (local batIcons
	 {
	  :discharging
	  {
	   :10	       "mdi_battery_10"
	   :20	       "mdi_battery_20"
	   :30	       "mdi_battery_30"
	   :40	       "mdi_battery_40"
	   :50	       "mdi_battery_50"
	   :60	       "mdi_battery_60"
	   :70	       "mdi_battery_70"
	   :80	       "mdi_battery_80"
	   :90	       "mdi_battery_90"
	   :alert      "mdi_battery_alert"
	   :full       "mdi_battery"
	   }
	  :charging
	  {
	   :10	       "mdi_battery_charging_10"
	   :20	       "mdi_battery_charging_20"
	   :30	       "mdi_battery_charging_30"
	   :40	       "mdi_battery_charging_40"
	   :50	       "mdi_battery_charging_50"
	   :60	       "mdi_battery_charging_60"
	   :70	       "mdi_battery_charging_70"
	   :80	       "mdi_battery_charging_80"
	   :90	       "mdi_battery_charging_90"
	   :full       "mdi_battery_charging_100"
	   :alert      "mdi_battery_alert"
	   }
	  }
	 )

  (local bat battery)

  (fn isCharging []
    (if (. bat :chargingQ)
        :charging
        :discharging
        )
    )

(local charge (. bat :charge))
(if (> charge 9)
(if (> charge 10)
(if (> charge 20)
(if (> charge 30)
(if (> charge 40)
(if (> charge 50)
(if (> charge 60)
(if (> charge 70)
(if (> charge 80)
(if (> charge 90)
(if (>= charge 91)
(.. "batterystate is " (. (. batIcons (isCharging)) :full)))
(.. "batterystate is " (. (. batIcons (isCharging)) :90)))
(.. "batterystate is " (. (. batIcons (isCharging)) :80)))
(.. "batterystate is " (. (. batIcons (isCharging)) :70)))
(.. "batterystate is " (. (. batIcons (isCharging)) :60)))
(.. "batterystate is " (. (. batIcons (isCharging)) :50)))
(.. "batterystate is " (. (. batIcons (isCharging)) :40)))
(.. "batterystate is " (. (. batIcons (isCharging)) :30)))
(.. "batterystate is " (. (. batIcons (isCharging)) :20)))
(.. "batterystate is " (. (. batIcons (isCharging)) :10)))
(.. "batterystate is " (. (. batIcons (isCharging)) :alert)))
)


(for [i 1 100 5]
  (local fo-battery {:chargingQ true :charge i})
    (print i  (getBatIcon fo-battery))
)
  
(for [i 1 100 5]
  (local fo-battery {:chargingQ false :charge i})
    (print i  (getBatIcon fo-battery))
)
;;  (local nerd wezterm.nerdfonts)
