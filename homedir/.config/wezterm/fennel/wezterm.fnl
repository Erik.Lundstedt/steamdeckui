;; -*- indent-tabs-mode: allways; tab-width: 2; -*-
(local theme (require :theme ))

(local colours  theme.colours)
(local myTheme  theme.myTheme)
(local mycolors theme.mycolors)


;;; description
;;this is the configuration file for wezterm terminal emulator
;;

(local wezterm (require :wezterm))

;;(local theme :PaleNighthC)

                    ;   "Andromeda")

;;; launch menu variable setup
(local launchMenu
             [
                {
                 :label "htop"
                 :args ["htop"]
                 ;;:cwd ;;working directory
                 }
                {
                 :label "xplr"
                 :args ["xplr"]
                 ;;:cwd ;;working directory
                 }
                ]
             )

;;; fonts
(local fontUsed
             (wezterm.font_with_fallback
                [
                 "UbuntuMono NF"
                 "JetBrains Mono"
                 ;;{:weight "Bold" :italic true}
                 ]
                )
             )




;;; :
(local ncpamixer
             {
                :label "volumectrl"
                :args ["ncpamixer"]
                })

(local mopidy
             {
                :label "music daemon"
                :args ["mopidy"]
                })

(local musicplayer
             {
                :label "musicplayer"
                :args ["ncmpcpp"]
                })
(local cava
             {
                :label "audio visualiser"
                :args ["cava"]
                }
             )
(local musicAction
             {
                :Multiple
                [
                 (wezterm.action {:SpawnCommandInNewTab mopidy})
                 ;;(wezterm.action {:ActivateTab 1})
                 (wezterm.action {:SpawnCommandInNewTab musicplayer})
                 (wezterm.action {:SpawnCommandInNewTab cava})
                 ]
                }
             )




;;; keybinds
(fn keybindings []
        "https://wezfurlong.org/wezterm/config/keys.html#configuring-key-assignments
         https://wezfurlong.org/wezterm/config/lua/keyassignment/index.html"
        (local wezsuper "SHIFT|CTRL")
    [
     ;;{:key "m" :mods "SUPER" :action "DisableDefaultAssignment"}
     {:key "PageUp"   :mods "ALT" :action (wezterm.action {:ActivateTabRelative 1})}
     {:key "PageDown" :mods "ALT" :action (wezterm.action {:ActivateTabRelative (- 1)})}

     {:key "LeftArrow"   :mods wezsuper :action (wezterm.action {:ActivateTabRelative (- 1)})}
     {:key "RightArrow"  :mods wezsuper :action (wezterm.action {:ActivateTabRelative 1})}
     {:key "m"           :mods wezsuper :action musicAction}
     {:key "9"           :mods "CTRL"   :action "IncreaseFontSize"}

     {:key "BrowserBack"	  :action (wezterm.action {:ActivateTabRelative (- 1)})}
     {:key "BrowserForward" :action (wezterm.action {:ActivateTabRelative 1})}
     ]
    )


;;wezterm.nerdfonts.
;;; code to grab the correct battery icons acording to state
(fn getBatIcon [battery]
  "TODO write something here"
    (local batIcons
{
:discharging
{
:10 wezterm.nerdfonts.mdi_battery_10
:20 wezterm.nerdfonts.mdi_battery_20
:30 wezterm.nerdfonts.mdi_battery_30
:40 wezterm.nerdfonts.mdi_battery_40
:50 wezterm.nerdfonts.mdi_battery_50
:60 wezterm.nerdfonts.mdi_battery_60
:70 wezterm.nerdfonts.mdi_battery_70
:80 wezterm.nerdfonts.mdi_battery_80
:90 wezterm.nerdfonts.mdi_battery_90
:alert wezterm.nerdfonts.mdi_battery_alert
:full  wezterm.nerdfonts.mdi_battery
}
:charging
{
:10    wezterm.nerdfonts.mdi_battery_charging_10
:20    wezterm.nerdfonts.mdi_battery_charging_20
:30    wezterm.nerdfonts.mdi_battery_charging_30
:40    wezterm.nerdfonts.mdi_battery_charging_40
:50    wezterm.nerdfonts.mdi_battery_charging_50
:60    wezterm.nerdfonts.mdi_battery_charging_60
:70    wezterm.nerdfonts.mdi_battery_charging_70
:80    wezterm.nerdfonts.mdi_battery_charging_80
:90    wezterm.nerdfonts.mdi_battery_charging_90
:full  wezterm.nerdfonts.mdi_battery_charging_100
:alert wezterm.nerdfonts.mdi_battery_alert
}
}
)

(local bat battery)

  (fn isCharging []
    (if (. bat :chargingQ) :charging :discharging))
;;;;; this could have been a swich-case statement, but it isnt
(local message (if (= (. bat :message) nil) "bat: "))
(local charge (. bat :charge))
 (if (> charge 9)
 (if (> charge 10)
 (if (> charge 20)
 (if (> charge 30)
 (if (> charge 40)
 (if (> charge 50)
 (if (> charge 60)
 (if (> charge 70)
 (if (> charge 80)
 (if (> charge 90)
 (if (>= charge 91)
 (.. message (. (. batIcons (isCharging)) :full)))
 (.. message (. (. batIcons (isCharging)) :90)))
 (.. message (. (. batIcons (isCharging)) :80)))
 (.. message (. (. batIcons (isCharging)) :70)))
 (.. message (. (. batIcons (isCharging)) :60)))
 (.. message (. (. batIcons (isCharging)) :50)))
 (.. message (. (. batIcons (isCharging)) :40)))
 (.. message (. (. batIcons (isCharging)) :30)))
 (.. message (. (. batIcons (isCharging)) :20)))
 (.. message (. (. batIcons (isCharging)) :10)))
 (.. message (. (. batIcons (isCharging)) :alert))))





;; (for [i 1 100 5]
;;   (local fo-battery {:chargingQ true :charge i})
;;     (print i	 (getBatIcon fo-battery))
;; )

;; (for [i 1 100 5]
;;   (local fo-battery {:chargingQ false :charge i})
;;     (print i	 (getBatIcon fo-battery))
;; )
;;  (local nerd wezterm.nerdfonts)
;;; set the style of the statusbar

(fn stateToIsCharging [state]

    (if (= state "Charging") true)
    (if (= state "Discharging") false)
    (if (= state "Empty" ) true)
    (if (= state "Full") false)
;; Unknown"
)




(wezterm.on :update-right-status
 (fn [window pane]
     (let [cells {}]
         (var cwd-uri (pane:get_current_working_dir))
         (when cwd-uri
             (set cwd-uri (cwd-uri:sub 8))
             (local slash (cwd-uri:find "/"))
             (var cwd "")
             (var hostname "")
             (when slash
                 (set hostname (cwd-uri:sub 1 (- slash 1)))
                 (local dot (hostname:find "[.]"))
                 (when dot
                     (set hostname (hostname:sub 1 (- dot 1))))
                 (set cwd (cwd-uri:sub slash))
                 (table.insert cells cwd)
                 ;;(table.insert cells hostname)
                 ))
         (local date
                        (.. wezterm.nerdfonts.mdi_clock "  "
                                (wezterm.strftime "%a %b %-d %H:%M")))
         (table.insert cells date)
         ;;TODO change to function that gives a nicer indicator
         (each [_ b (ipairs (wezterm.battery_info))]
             (local fo-battery
                            {
                             :isCharging (stateToIsCharging b.state)
                             :charge (* b.state_of_charge 100)}
                            )
             (local batstatus
                            (..
                             ;;wezterm.nerdfonts.mdi_battery "  "
                             (getBatIcon fo-battery)
                             ;;b.state
                             " "
                             (string.format "%.0f%%" (* b.state_of_charge 100)
                                                                 )
                                    )
                            )
                        (table.insert cells batstatus)
            )

        (local LEFT_ARROW (utf8.char 57523))
        (local SOLID_LEFT_ARROW (utf8.char 57522))
        (local colors ["#3c1361"
                                     "#52307c"
                                     "#663a82"
                                     "#7c5295"
                                     "#b491c8"])
        (local text-fg "#c0c0c0")
        (local elements {})
        (var num-cells 0)
                (local push
                             (fn [text is-last]
                                 (let [cell-no (+ num-cells 1)]
                                     (table.insert elements
                                                                 {:Foreground
                                                                    {:Color text-fg
                                                                     }
                                                                    }
                                                                 )
                                     (table.insert elements
                                                                 {:Background
                                                                    {:Color
                                                                     (. colors cell-no)
                                                                     }
                                                                    }
                                                                 )
                                     (table.insert elements
                                                                 {:Text
                                                                    (.. " " text " ")})
                                     (when (not is-last)
                                         (table.insert elements
                                                                     {
                                                                        :Foreground
                                                                        {
                                                                         :Color
                                                                         (. colors
                                                                                (+ cell-no 1)
                                                                                )
                                                                         }
                                                                        }
                                                                     )
                                         (table.insert elements {:Text SOLID_LEFT_ARROW}))
                                 (set num-cells (+ num-cells 1)))))
                (while (> (length cells) 0)
                        (local cell (table.remove cells 1))
                        (push cell (= (length cells) 0)))
                (window:set_right_status (wezterm.format elements)))))









(var opacity 0.8)
(var scrollbar false)
;;(var textBgOpacity 0.9)

(var exit {:close :Close :hold :Hold :closeOnClean :CloseOnCleanExit})



;;; this is equivalent to a return statement
{:keys (keybindings)
 :launch_menu launchMenu
 ;; :color_scheme theme;;dont use
 :font fontUsed
 :font_size 16
 :colors colours
 :window_background_opacity opacity
 ;; :text_background_opacity textBgOpacity
 :enable_scroll_bar scrollbar
 :use_fancy_tab_bar false
 :exit_behavior (. exit :close)}
