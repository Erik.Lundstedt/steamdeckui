local theme = require("theme")
local colours = theme.colours
local myTheme = theme.myTheme
local mycolors = theme.mycolors
local wezterm = require("wezterm")
local launchMenu = {{label = "htop", args = {"htop"}}, {label = "xplr", args = {"xplr"}}}
local fontUsed = wezterm.font_with_fallback({"UbuntuMono NF", "JetBrains Mono"})
local ncpamixer = {label = "volumectrl", args = {"ncpamixer"}}
local mopidy = {label = "music daemon", args = {"mopidy"}}
local musicplayer = {label = "musicplayer", args = {"ncmpcpp"}}
local cava = {label = "audio visualiser", args = {"cava"}}
local musicAction = {Multiple = {wezterm.action({SpawnCommandInNewTab = mopidy}), wezterm.action({SpawnCommandInNewTab = musicplayer}), wezterm.action({SpawnCommandInNewTab = cava})}}
local function keybindings()
  local wezsuper = "SHIFT|CTRL"
  return {{key = "PageUp", mods = "ALT", action = wezterm.action({ActivateTabRelative = 1})}, {key = "PageDown", mods = "ALT", action = wezterm.action({ActivateTabRelative = ( - 1)})}, {key = "LeftArrow", mods = wezsuper, action = wezterm.action({ActivateTabRelative = ( - 1)})}, {key = "RightArrow", mods = wezsuper, action = wezterm.action({ActivateTabRelative = 1})}, {key = "m", mods = wezsuper, action = musicAction}, {key = "9", mods = "CTRL", action = "IncreaseFontSize"}, {key = "BrowserBack", action = wezterm.action({ActivateTabRelative = ( - 1)})}, {key = "BrowserForward", action = wezterm.action({ActivateTabRelative = 1})}}
end
local function getBatIcon(battery)
  local batIcons = {discharging = {["10"] = wezterm.nerdfonts.mdi_battery_10, ["20"] = wezterm.nerdfonts.mdi_battery_20, ["30"] = wezterm.nerdfonts.mdi_battery_30, ["40"] = wezterm.nerdfonts.mdi_battery_40, ["50"] = wezterm.nerdfonts.mdi_battery_50, ["60"] = wezterm.nerdfonts.mdi_battery_60, ["70"] = wezterm.nerdfonts.mdi_battery_70, ["80"] = wezterm.nerdfonts.mdi_battery_80, ["90"] = wezterm.nerdfonts.mdi_battery_90, alert = wezterm.nerdfonts.mdi_battery_alert, full = wezterm.nerdfonts.mdi_battery}, charging = {["10"] = wezterm.nerdfonts.mdi_battery_charging_10, ["20"] = wezterm.nerdfonts.mdi_battery_charging_20, ["30"] = wezterm.nerdfonts.mdi_battery_charging_30, ["40"] = wezterm.nerdfonts.mdi_battery_charging_40, ["50"] = wezterm.nerdfonts.mdi_battery_charging_50, ["60"] = wezterm.nerdfonts.mdi_battery_charging_60, ["70"] = wezterm.nerdfonts.mdi_battery_charging_70, ["80"] = wezterm.nerdfonts.mdi_battery_charging_80, ["90"] = wezterm.nerdfonts.mdi_battery_charging_90, full = wezterm.nerdfonts.mdi_battery_charging_100, alert = wezterm.nerdfonts.mdi_battery_alert}}
  local bat = battery
  local function isCharging()
    if bat.chargingQ then
      return "charging"
    else
      return "discharging"
    end
  end
  local message
  if (bat.message == nil) then
    message = "bat: "
  else
    message = nil
  end
  local charge = bat.charge
  if (charge > 9) then
    if (charge > 10) then
      if (charge > 20) then
        if (charge > 30) then
          if (charge > 40) then
            if (charge > 50) then
              if (charge > 60) then
                if (charge > 70) then
                  if (charge > 80) then
                    if (charge > 90) then
                      if (charge >= 91) then
                        return (message .. batIcons[isCharging()].full)
                      else
                        return nil
                      end
                    else
                      return (message .. batIcons[isCharging()]["90"])
                    end
                  else
                    return (message .. batIcons[isCharging()]["80"])
                  end
                else
                  return (message .. batIcons[isCharging()]["70"])
                end
              else
                return (message .. batIcons[isCharging()]["60"])
              end
            else
              return (message .. batIcons[isCharging()]["50"])
            end
          else
            return (message .. batIcons[isCharging()]["40"])
          end
        else
          return (message .. batIcons[isCharging()]["30"])
        end
      else
        return (message .. batIcons[isCharging()]["20"])
      end
    else
      return (message .. batIcons[isCharging()]["10"])
    end
  else
    return (message .. batIcons[isCharging()].alert)
  end
end
local function stateToIsCharging(state)
  if (state == "Charging") then
  else
  end
  if (state == "Discharging") then
  else
  end
  if (state == "Empty") then
  else
  end
  if (state == "Full") then
    return false
  else
    return nil
  end
end
local function _18_(window, pane)
  local cells = {}
  local cwd_uri = pane:get_current_working_dir()
  if cwd_uri then
    cwd_uri = cwd_uri:sub(8)
    local slash = cwd_uri:find("/")
    local cwd = ""
    local hostname = ""
    if slash then
      hostname = cwd_uri:sub(1, (slash - 1))
      local dot = hostname:find("[.]")
      if dot then
        hostname = hostname:sub(1, (dot - 1))
      else
      end
      cwd = cwd_uri:sub(slash)
      table.insert(cells, cwd)
    else
    end
  else
  end
  local date = (wezterm.nerdfonts.mdi_clock .. "  " .. wezterm.strftime("%a %b %-d %H:%M"))
  table.insert(cells, date)
  for _, b in ipairs(wezterm.battery_info()) do
    local fo_battery = {isCharging = stateToIsCharging(b.state), charge = (b.state_of_charge * 100)}
    local batstatus = (getBatIcon(fo_battery) .. " " .. string.format("%.0f%%", (b.state_of_charge * 100)))
    table.insert(cells, batstatus)
  end
  local LEFT_ARROW = utf8.char(57523)
  local SOLID_LEFT_ARROW = utf8.char(57522)
  local colors = {"#3c1361", "#52307c", "#663a82", "#7c5295", "#b491c8"}
  local text_fg = "#c0c0c0"
  local elements = {}
  local num_cells = 0
  local push
  local function _22_(text, is_last)
    local cell_no = (num_cells + 1)
    table.insert(elements, {Foreground = {Color = text_fg}})
    table.insert(elements, {Background = {Color = colors[cell_no]}})
    table.insert(elements, {Text = (" " .. text .. " ")})
    if not is_last then
      table.insert(elements, {Foreground = {Color = colors[(cell_no + 1)]}})
      table.insert(elements, {Text = SOLID_LEFT_ARROW})
    else
    end
    num_cells = (num_cells + 1)
    return nil
  end
  push = _22_
  while (#cells > 0) do
    local cell = table.remove(cells, 1)
    push(cell, (#cells == 0))
  end
  return window:set_right_status(wezterm.format(elements))
end
wezterm.on("update-right-status", _18_)
local opacity = 0.8
local scrollbar = false
local exit = {close = "Close", hold = "Hold", closeOnClean = "CloseOnCleanExit"}
return {keys = keybindings(), launch_menu = launchMenu, font = fontUsed, font_size = 16, colors = colours, window_background_opacity = opacity, enable_scroll_bar = scrollbar, use_fancy_tab_bar = false, exit_behavior = exit.close}
