#!/usr/bin/env fennel
(local dsl {})



(fn dsl.kvpair [key value]
  (.. key " = " value)
  )


(fn dsl.kvpairs [tbl]
  (local retval {})
  (each [k v (pairs tbl)]
    (table.insert retval (dsl.kvpair (. v :k) (. v :v)))
    )
  (table.concat retval "\n")
  )


(fn dsl.autorun [ws program]
  (.. "autorun = " "ws[" ws "]: " program )
  )
(fn dsl.autostartProgs [programs]
  (local retval {})
  (each [k v (pairs programs)]
    (table.insert retval (dsl.autorun (. v :ws) (. v :program)))
    )
  (table.concat retval "\n")
  )


;;(fn dsl.layout [ws config]
;;  (.. "autorun = " "ws[" ws "]: " program ))

(fn dsl.program [alias program]
  (.. "program" "[" alias "] =" program)
  )

(fn dsl.programs [progs]
  (local retval {})
  (each [k v (pairs progs)]
    (table.insert retval (dsl.program (. v :alias) (. v :program)))
    )
  (table.concat retval "\n")
  )



(fn dsl.bind [keys action]
  (.. "bind" "[" action "] = " "MOD+" keys  )
  )
(fn dsl.binds [keybinds]
  (local retval {})
  (each [k v (pairs keybinds)]
    (table.insert retval (dsl.bind (. v :keys) (. v :action)))
    )
  (table.concat retval "\n")
  )


(fn dsl.rule [matcher rules]
  (.. "quirk[" matcher "] = " (table.concat rules "+"))
  )


(fn dsl.rules [rules]
  (local retval {})
  (each [k v (pairs rules)]
    (table.insert retval (dsl.rule (. v :matcher) (. v :rules)))
    )
  (table.concat retval "\n")
  )



(fn dsl.preview [content]
  (print content)
)


(fn dsl.writeFile [content filename]
  (with-open [fout (io.open filename :w) ]
    (fout:write content)
    ) ; => first line of index.html
  (print (.. "wrote " "data " "to " filename))
  )

;;{:dsl dsl :feed feed}
dsl
