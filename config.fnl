#!/usr/bin/env fennel

(local dsl (require :dsl))
(local helper (require :helper))

(fn hexcolour [hex]
  (.. "\\#" hex)
  )




(local barsettings
       (dsl.kvpairs
        [
         {:k "bar_action_expand"       :v 0}
         {:k "bar_at_bottom"           :v 0}
         {:k "bar_border_width"        :v 1}
         {:k "bar_enabled"             :v 1}
;;         {:k "bar_enabled_ws[1]"       :v 0}
         {:k "bar_action"              :v "baraction.sh"}
         {:k "bar_border[1]"           :v (hexcolour "8099ee")}
         {:k "bar_border_unfocus[1]"   :v (hexcolour "242837")}
         {:k "bar_color[1]"            :v (hexcolour "963c59")}
         {:k "bar_color_selected[1]"   :v "rgb:00/80/80"}
         {:k "bar_font"                :v "xos4 Terminus:pixelsize=14:antialias=true"}
         {:k "bar_font_color[1]"       :v "rgb:a0/a0/a0"}
         {:k "bar_font_color_selected" :v "black"}
         {:k "bar_font_pua"            :v "Typicons:pixelsize=14:antialias=true"}
         {:k "bar_format"              :v "+N:+I +S <+D>+4<%a %b %d %R %Z %Y+8<+A+4<+V"}
         {:k "bar_justify"             :v "center"}
         ]))

(local settings
       (dsl.kvpairs
        [
         {:k "workspace_indicator"     :v "listcurrent,listactive,markcurrent,printnames"       }
         {:k "keyboard_mapping"        :v "~/.config/spectrwm/spectrwm_se.conf"}
         {:k "clock_format"            :v "%a %b %d %R %Z %Y" }
         {:k "modkey"                  :v "mod4"}
         {:k "layout"                  :v "ws[1]:0:0:0:0:fullscreen"}
         {:k "stack_enabled"           :v 1 }
         {:k "clock_enabled"           :v 1 }
         {:k "iconic_enabled"          :v 0 }
         {:k "maximize_hide_bar"       :v 1 }
         {:k "window_class_enabled"    :v 1 }
         {:k "window_instance_enabled" :v 0 }
         {:k "window_name_enabled"     :v 1 }
         {:k "verbose_layout"          :v 1 }
         {:k "urgent_enabled"          :v 1 }
         {:k "border_width"            :v 1 }
         ]))


;; layout		= ws[1]:4:0:0:0:vertical
;; layout		= ws[2]:0:0:0:0:horizontal
;; layout		= ws[3]:0:0:0:0:fullscreen
;; layout		= ws[4]:4:0:0:0:vertical_flip
;; layout		= ws[5]:0:0:0:0:horizontal_flip




;; urgent_collapse	= 0



(local autostart
       (dsl.autostartProgs
        [
         ;;{:ws 1 :program "xbindkeys -fg ~/.config/xbindkeys/xbindkeysrc.scm&"}
;;         {:ws 1 :program "xrandr --output DP-0 --off"}
;;         {:ws 1 :program "xrandr --output DP-1 --off"}
;;         {:ws 1 :program "xrandr --output DVI-D-0 --off"}
;;         {:ws 1 :program "xrandr --output DVI-D-1 --off"}
         {:ws 2 :program "picom&"}
         {:ws 2 :program "nitrogen --set-auto --random"}
         {:ws 2 :program "sleep 2"}
         {:ws 1 :program "steam -gamepadui&"}
         {:ws 2 :program "tym&"}
         ;;{:ws 1 :program "tym -e \"xwallpaper --center ~/wallpapers/wallpaper\"&"}
         ;;{:ws 1 :program "hsetroot -full ~/wallpapers/wallpaper&"}
         ;;{:ws 5 :program "emacs&"}


         ])
       )

(local programs
       (dsl.programs
        [
         ;;        {:alias :program}
         {:alias "altTerm"         :program "tym"}
         {:alias "browser"         :program "luakit"}
         {:alias "term"            :program "wezterm"}
         {:alias "lock"            :program "i3lock -c 303099"}
         {:alias "menu"            :program "rofi -show run $dmenu_bottom -fn $bar_font -nb $bar_color -nf $bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected"}
         {:alias "search"          :program "rofi -dmenu $dmenu_bottom -i -fn $bar_font -nb $bar_color -nf $bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected"}
         {:alias "nameworkspace"   :program "rofi -dmenu $dmenu_bottom -p Workspace -fn $bar_font -nb $bar_color -nf $bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected"}
         ]
        )
       )

(local keyBindings
       (dsl.binds
       [
        {:keys "f"              :action "fullscreen_toggle"}
        {:keys "Shift+f"        :action "maximize_toggle"}
        {:keys "Return"         :action "term"}
        {:keys "Shift+Return"   :action "altTerm"}
        {:keys "Shift+b"        :action "browser"}
        ;; {:keys "" :action ""}
        ;; {:keys "" :action ""}
        ]
       )
       )

;;(table.insert retval (dsl.rule (. v :matcher) (. v :rules)))
;;matcher syntax is strange:
;;[class[:instance[:name]]]


(local rules
       (dsl.rules
        [
         {:matcher "float"      :rules [helper.rules.float]}
         {:matcher ".*:float"   :rules [helper.rules.float]}
         {:matcher "Steamwebhelper:steamwebhelper:SP"   :rules [helper.rules.fullscreen]}
         {:matcher "steamwebhelper:Steamwebhelper:SP"   :rules [helper.rules.fullscreen]}
         ]))


(local cfg
       (table.concat
        [
         autostart
         barsettings
         settings
         programs
         keyBindings
         rules
         ] "\n")
       )
;;(dsl.preview cfg)
(dsl.writeFile cfg "spectrwm.conf")
