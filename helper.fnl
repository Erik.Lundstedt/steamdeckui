#!/usr/bin/env fennel
(local helper {})

(local rules
       {
        :float "FLOAT"
        :fullscreen "FULLSCREEN"
        :fs "FULLSCREEN"
        :focusPrev "FOCUSPREV"
        :none "NONE"
        :disable "NONE"
        })

(tset helper :rules rules)

helper
